﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class GameDataEditor : EditorWindow {

    public GameData gameData;
   // private string gameDataProjectFilePath = "/StreamingAssets/";
    // Use this for initialization
   
	
    [MenuItem("Window/Question Data Editor")]
    static void Init()
    {
        GameDataEditor window = (GameDataEditor)EditorWindow.GetWindow(typeof(GameDataEditor));
        window.Show();
    }

    private void OnGUI()
    {
      //   if (gameData!=null)
         //{
            
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("gameData");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();
            if (GUILayout.Button("Save Data"))
            {
                SaveGameData();
            }
       // }

        if (GUILayout.Button("Load Data"))
        {
            LoadGameData();
        }
    }
    private void LoadGameData()
    {
        string filePath = Application.dataPath + "/StreamingAssets/Question" + gameData.questionIndex.ToString() + ".json";

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
            gameData = new GameData();
        }
       
    }
    private void SaveGameData()
    {
        string dataAsJson = JsonUtility.ToJson(gameData);
        string filePath = Application.dataPath + "/StreamingAssets/Question" + gameData.questionIndex.ToString() + ".json";

        File.WriteAllText(filePath,dataAsJson);
    }
}
