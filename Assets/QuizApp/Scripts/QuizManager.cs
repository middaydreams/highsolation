﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{
    //private DataController dataController;
    private QuestionData questionData;
    private GameData gameData;
    [SerializeField]
    GameObject answerButtonPrefab;
    private int questionIndex = 1;
    [SerializeField]
    Text questionText;
    public Transform answerButtonsParent;
    public Text ScoreText;
    [SerializeField]
    private int Score;
    public Text answerText, correctAnswerText;
    private int totalScore=0;
    List<GameObject> answerButtonObjects = new List<GameObject>();
    // Use this for initialization
    void Start()
    {      
        Score = 10;
        ShowQuestion();
    }

    private string correctAnswer;
    public void ShowQuestion()
    {
        gameData = DataController.Instance.GetCurrentdata(questionIndex);
        questionData = gameData.QuestionData;
        //questionIndex = gameData.questionIndex;
        RemoveAnswerButtons();
        QuestionData question = questionData;
        correctAnswer = GetCorrectAnswer(question);
        questionText.text = question.questionText;
        for (int i = 0; i < question.answers.Length; i++)
        {

            GameObject GO = Instantiate(answerButtonPrefab, transform.position, Quaternion.identity);
            answerButtonObjects.Add(GO);
            answerButtonObjects[i].gameObject.transform.SetParent(answerButtonsParent);
            answerButtonObjects[i].transform.localScale = Vector3.one;
            AnswerButton answerButton = answerButtonObjects[i].GetComponent<AnswerButton>();
            answerButton.SetUp(question.answers[i]);

        }
    }

    public void AnswerButtonClicked(string answer)
    {


        //if (isCorrect)
        //{
        //    answerText.text = "Correct";
        //    CountScore(Score);
        //}
        //else
        //{
        //    answerText.text = "Wrong";
        //    correctAnswerText.text = "Correct Answer: " + correctAnswer;
        //}
        answerText.text = answer;
        for (int i = 0; i < answerButtonObjects.Count; i++)
        {
            answerButtonObjects[i].GetComponent<Button>().interactable = false;
        }



    }

    public string GetCorrectAnswer(QuestionData data)
    {
        string correctAnswer = "";
        if (data == null)
        {
            return null;
        }
        else
        {
            for (int i = 0; i < data.answers.Length; i++)
            {
                if (data.answers[i].isCorrect == true)
                {
                    correctAnswer = data.answers[i].answerText;
                }
            }
        }

        return correctAnswer;
    }
    public void RemoveAnswerButtons()
    {
        for (int i = 0; i < answerButtonObjects.Count; i++)
        {
            answerButtonObjects[i].transform.SetParent(null);
            Destroy(answerButtonObjects[i]);

        }
        while (answerButtonObjects.Count > 0)
        {

            answerButtonObjects.RemoveAt(0);

        }
        //Debug.Log(answerButtonObjects.Count);
    }
    public void OnPressNext()
    {
        questionIndex++;
        ResetTextField();

        ShowQuestion();

    }

    public void CountScore(int Score)
    {
        totalScore += Score;
        ScoreText.text = "Score: " + totalScore.ToString();
    }
    public void ResetTextField()
    {
        answerText.text = "";
        correctAnswerText.text = "";
    }
}
