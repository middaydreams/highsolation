﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public int hour = 0;
    public int minutes = 0;
    public int seconds = 0;
    //-- time speed factor
    ///public float clockSpeed = 1.0f;     // 1.0f = realtime, < 1.0f = slower, > 1.0f = faster
   // int totalSeconds = 0;
    //-- internal vars
    //float msecs = 0;
    // Start is called before the first frame update
    void Start()
    {
        //totalSeconds = (hour * 60 * 60) + (minutes * 60) + seconds;
        StartCoroutine(StartTimer());
    }

    // Update is called once per frame
    void Update()
    {
        //-- calculate time
        //msecs += Time.deltaTime * clockSpeed;
        //if (msecs >= 1.0f)
        //{
        //    msecs -= 1.0f;
        //    seconds++;
        //    if (seconds >= 60)
        //    {
        //        seconds = 0;
        //        minutes++;
        //        if (minutes > 60)
        //        {
        //            minutes = 0;
        //            hour++;
        //            if (hour >= 24)
        //                hour = 0;
        //        }
        //    }
        //}
        //if (hour>0)
        //{
        //    if (minutes>0)
        //    {
        //        if (seconds>0)
        //        {

        //        }
        //    }
        //}

        ////-- calculate pointer angles
        //float rotationSeconds = (360.0f / 60.0f) * seconds;
        //float rotationMinutes = (360.0f / 60.0f) * minutes;
        //float rotationHours = ((360.0f / 12.0f) * hour) + ((360.0f / (60.0f * 12.0f)) * minutes);
    }

    IEnumerator StartTimer()
    {
        yield return null;
        while (true)
        {
            if (hour==0 && minutes==0 && seconds==0)
            {
                yield break;
            }
            yield return new WaitForSeconds(1);
          
            if (seconds > 0)
            {
                seconds -= 1;
            }
            else
            {
                seconds = 60;
                minutes -= 1;
                if (minutes == 0)
                {
                    minutes = 59;
                    if (hour != 0)
                    {
                        hour -= 1;
                    }
                }
            }
            Debug.Log("Show Time="+hour+":"+minutes+":"+seconds);
        }

      
        
       
    }
}
