﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TodoItemManager : MonoBehaviour
{
    public InputField nameText;
    public InputField fromTimeText;
    public InputField toTimeText;
    public Toggle fromAmPmToggle;
    public Toggle toAmPmToggle;
    string todoName;
    string todoTime;
    string fromampm;
    string toampm;
    public GameObject itemPrefab;
    public GameObject itemParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnClickSaveButton()
    {
       
        toampm = toAmPmToggle.isOn ? "pm" : "am";
        fromampm = fromAmPmToggle.isOn ? "pm" : "am";
        todoName = nameText.text;
        todoTime = fromTimeText.text +fromampm+ "-" + toTimeText.text+toampm;
        GameObject tempItemPrefab = Instantiate(itemPrefab, itemPrefab.transform.position, Quaternion.identity,itemParent.transform);
        tempItemPrefab.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = todoName;
        tempItemPrefab.transform.GetChild(1).gameObject.GetComponent<Text>().text = todoTime;
    }
}
