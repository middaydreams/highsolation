﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirectoryDataContainer : MonoBehaviour
{
    public List<DirectoryData> directoryDatas= new List<DirectoryData>();
    public Text descriptionText;

    void Start()
    {
       
    }
   
    public void OnPressButton(int index)
    {
        if (directoryDatas==null)
        {
            return;
        }
        
        descriptionText.text = directoryDatas[index].description;
        
    }
}

[System.Serializable]
public class DirectoryData
{
    public string description;
}